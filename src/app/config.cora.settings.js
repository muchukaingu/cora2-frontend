(function() {
    'use strict';

    angular
        .module('app')
        .provider('coraSettings', SettingsProvider);

    /* @ngInject */
    function SettingsProvider() {
        // Provider
        var settings = {
            API: '',
            modelConfig: ''
        };

        this.setAPI = setAPI;
        this.setModelConfig = setModelConfig;




        function setAPI(api) {
            settings.API = api;
        }
        function setModelConfig(config) {
            settings.modelConfig = config;
        }



        // Service
        this.$get = function() {
            return {
                API: settings.API,
                modelConfig: settings.modelConfig
            };
        };
    }
})();
