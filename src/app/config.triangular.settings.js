(function() {
    'use strict';

    angular
        .module('app')
        .config(translateConfig);

    /* @ngInject */
    function translateConfig(triSettingsProvider, triRouteProvider, coraSettingsProvider, $resourceProvider, csrfCDProvider, $httpProvider) {
        var now = new Date();
        // set app name & logo (used in loader, sidemenu, footer, login pages, etc)
        triSettingsProvider.setName('cora');
        triSettingsProvider.setCopyright('&copy; 2011 - ' + now.getFullYear() + ' GriffinWorx | All rights reserved');
        triSettingsProvider.setLogo('assets/images/logo-ultra-light.png');
        triSettingsProvider.setAppLogo('assets/images/app_logo.png');
        triSettingsProvider.setSponsors('assets/images/sponsor-logos.png');
        // set current version of app (shown in footer)
        triSettingsProvider.setVersion('1.0.0');
        // set the document title that appears on the browser tab
        triRouteProvider.setTitle('GriffinWorx Competition Management Platform');
        triRouteProvider.setSeparator('|');
        //set the API baseURL
        coraSettingsProvider.setAPI('http://circuit.cloudapp.net:3003/api/');
        coraSettingsProvider.setModelConfig('http://circuit.cloudapp.net:3003/explorer/swagger.json');
        // coraSettingsProvider.setAPI('http://localhost:4000/api/');
        // coraSettingsProvider.setModelConfig('http://localhost:4000/explorer/swagger.json');
        $resourceProvider.defaults.stripTrailingSlashes = false;
        $httpProvider.defaults.xsrfCookieName = 'x-csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';



        $httpProvider.defaults.transformRequest.push(function(data, headersGetter) {
            var currentHeaders = headersGetter();
            // angular.extend(currentHeaders, {'X-CSRFToken':'xxx'});
            //console.info("Current headers", currentHeaders);
            return data;
          });

    }
})();
