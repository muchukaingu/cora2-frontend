(function() {
    'use strict';

    angular
        .module('app.cora')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {
        $stateProvider

        .state('triangular.admin-countries', {
            url: '/countries',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.admin-centres', {
            url: '/centres',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.admin-competitions', {
            url: '/competitions',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.admin-stages', {
            url: '/stages',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })


        .state('triangular.competition-competitionstages', {
            url: '/competitons/competitionstages',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-entrepreneurs', {
            url: '/competitions/entrepreneurs',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.admin-submissions', {
            url: '/competitions/submissions',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-judges', {
            url: '/competitions/judges',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-mentors', {
            url: '/competitions/mentors',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-coachingsessions', {
            url: '/competitions/coachingsessions',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-pitchingsessions', {
            url: '/competitions/pitchingsessions',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.competition-submissionforms', {
            url: '/competitions/submissionforms',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'SubmissionFormController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.users-administrators', {
            url: '/users/administrators',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.users-managers', {
            url: '/users/managers',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.users-entrepreneurs', {
            url: '/users/entrepreneurs',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.users-judges', {
            url: '/users/judges',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.users-mentors', {
            url: '/users/mentors',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        })
        .state('triangular.entrepreneur-submissions', {
            url: '/entrepreneur/submissions',
            templateUrl: 'app/cora/admin-ui/admin-ui.tmpl.html',
            controller: 'AdminUIController',
            controllerAs: 'vm',
            // layout-column class added to make footer move to
            // bottom of the page on short pages
            data: {
                layout: {
                    contentClass: 'layout-column'
                }
            }
        });














    }
})();
