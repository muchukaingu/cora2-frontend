(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .controller('ProfileController', ProfileController);

    /* @ngInject */
    function ProfileController($q, SessionService, AdminUIService, $mdToast, $location, $window) {
        var vm = this;
        vm.formPreview = {
          inputs:[],
          textAreas:[]
        };
        vm.loggedInUser = SessionService.getUser();
        vm.user = {};
        vm.model = {};
        vm.form = {};
        vm.edit = false;
        vm.inputs = [];
        vm.selects = [];
        vm.predefinedSelects = [];
        vm.competitionName = "";


        vm.submit = submit;



        vm.activate = activate;
        vm.getSubmissionForm = getSubmissionForm;
        vm.getSubmission = getSubmission;
        vm.updateProfile = updateProfile;
        vm.createFields = createFields;
        vm.getUserModel = getUserModel;
        vm.getPlural = getPlural;
        vm.setCompetitionName = setCompetitionName;

        activate();

        function submit(){
          vm.submission.entrepreneurId = vm.loggedInUser.id;
          vm.edit?vm.resource = AdminUIService.update(vm.submission, "submissions"):vm.resource = AdminUIService.save(vm.submission, "submissions");
          vm.resource.then(
            function(res){
              vm.submission = res.data.data;
              $mdToast.show(
                  $mdToast.simple()
                  .content(vm.edit?'Thank you, '+vm.loggedInUser.firstname+'. Your Business Model has been updated.':'Thank you, '+vm.loggedInUser.firstname+'. Your Business Model has been submitted.')
                  .position('top right')
                  .highlightAction(true)
                  .hideDelay(3000)
              ).then(function() {
                $window.location.href = "http://"+$location.host();

              });
            },
            function(err){}
          )
        }

        function updateProfile(){
          vm.resource=AdminUIService.update(vm.user, "entrepreneurs");
          vm.resource.then(
            function(res){
              vm.user = res.data.data;
              $mdToast.show(
                  $mdToast.simple()
                  .content('Thank you, '+vm.loggedInUser.firstname+'. Your profile has been updated.')
                  .position('top right')
                  .highlightAction(true)
                  .hideDelay(3000)
              ).then(function() {
                  $state.go('authentication.login');
              });
            },
            function (err){

            }
          )
        }

        function activate(){
          vm.getSubmissionForm();
          vm.getSubmission();
          vm.getUserModel();
          vm.setCompetitionName();


        }

        function setCompetitionName(){

          if($location.host()=="ebay.startupcup.com"){
            vm.competitionName = "eBay StartUp Cup Challenge";
          }
          else if($location.host()=="orange.startupcup.com"){
            vm.competitionName = "Orange StartUp Cup Challenge";
          }
          else {
            vm.competitionName = "StartUp Cup Challenge";
          }
        }

        function getSubmissionForm(){
          vm.resource = AdminUIService.query({},"submissionforms");
          vm.resource.then(
            function(res){
              vm.forms = res.data.data;
              vm.form = vm.forms[0];
              //console.log(vm.form);
              angular.forEach(vm.form.fields, function(field){
                //console.log(field);
                if(field.type == "textArea"){
                  vm.formPreview.textAreas.push(field);
                }
                else if(field.type == "text"){
                  vm.formPreview.inputs.push(field);
                }
              });
              console.log(vm.formPreview);

            },
            function(err){

            }

          )
        }

        function getSubmission(){
          vm.resource = AdminUIService.query({},"submissions?filter[where][entrepreneurId]="+vm.loggedInUser.id);
          vm.resource.then(
            function(res){
              vm.submission = res.data.data[0];
              if(vm.submission !== null && vm.submission !== undefined){
                vm.edit = true;
                console.log(vm.submission);
              }


            },
            function(err){

            }

          )
        }
        //Used to get user model to create profile fields
        function getUserModel(){
          vm.resource = AdminUIService.query({id:vm.loggedInUser.id},vm.loggedInUser.type+"s" );
          vm.resource.then(
            function(res){
              vm.user = res.data.data[0];
              vm.model = res.data.model;
              console.log(vm.user);
              vm.controlsResource = vm.createFields();
              vm.controlsResource.then(
                function(){
                  console.log("showing modal");
                },
                function(){}
              );
            },
            function(error){

            }
          )
        }

        /**
         * @ngdoc method
         * @name getPlural
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the dialog in add or edit mode
         *
         *
         *

        */

        function getPlural(model){
          if (model.slice(-1)=="y"){
            return model = model.substring(0, model.length-1)+"ies".toLowerCase();

          }
          else {

            return model = model.toLowerCase() + "s";


          }
        }


        /**
         * @ngdoc method
         * @name createFields
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the dialog in add or edit mode
         *
         *
         *

        */

        function createFields() {
          console.log(vm.model);

          return $q(function(resolve, reject) {
            console.log(vm.model);

                angular.forEach(vm.model.properties, function(config, column){

                  console.log(column);

                  if(config.hasOwnProperty("editor_index")){


                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";
                      var objectType = config.data_source.substring(4, config.data_source.length-2);

                      switch(objectType) {
                          case "province":
                              refs = vm.provinces;
                              break;
                          case "district":
                              refs = vm.districts;
                              break;
                          case "town":
                              refs = vm.towns;
                              break;
                          case "ward":
                              refs = vm.wards;
                              break;
                          case "market":
                              refs = vm.markets;
                              break;
                          case "packagings":
                              refs = vm.packagings;
                              break;
                          case "crop":
                              refs = vm.crops;
                              break;
                          case "advertiser":
                              refs = vm.advertisers;
                              console.log(refs);
                              break;

                          default:
                              console.log("error");
                      }




                      vm.selects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});

                      //If we are in edit mode set the value of the selected reference, so that the name is visible to the user
                      if(vm.selected && vm.selected.length == 1){
                          angular.forEach(vm.selects, function(select){
                            vm.object.ref=vm.object[select.name]; //temporal fix. It will be a problem when there are multiple reference types. Find a fix to allow dynamic ng-model
                          })
                      }


                    }
                    else if (config.hasOwnProperty("choices")){
                      var refs = config.choices;
                      console.log(refs);


                      vm.predefinedSelects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});

                    }
                    else if (config.type == "date") {
                      if(vm.mode=="edit"){
                        console.log(vm.object[column]);
                        vm.object[column] = new Date(vm.object[column]);
                        console.log(vm.object[column]);
                      }
                      vm.dateControls.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }
                    else {
                      vm.inputs.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }

                  }
                });

                /* Candidate for deletion

                angular.forEach(vm.model.relations, function(config, column){
                 if (config.type == "belongsTo" || config.type == "hasOne" ){
                    var refs = [];
                    var pluralModel = getPlural(config.model);
                    vm.resource = AdminUIService.query({},pluralModel);
                    vm.resource.then(
                      function(res){
                        refs = res.data.data;
                        vm.selects.push({name: column+"Id", refs:refs, label:column});
                      },
                      function(err){

                      }
                    )

                  }

                });
                */

                resolve (vm);
                console.log(vm.inputs);
            });


        }





    }
})();
