(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .controller('SignupController', SignupController);

    /* @ngInject */
    function SignupController($q, $scope, $state, $mdToast, $http, $filter, triSettings, AuthService, AdminUIService, $location, SiteInfoService) {
        var vm = this;
        vm.siteURL = SiteInfoService.getSiteInfo().siteURL;
        vm.competitionName = SiteInfoService.getSiteInfo().competitionName;
        vm.registrationFailed = false;
        vm.errorMessage = "";
        vm.centres = [];
        vm.triSettings = triSettings;
        vm.signupClick = signupClick;
        vm.registering = false
        vm.user = {
            firstname: '',
            lastname:'',
            email: '',
            password: '',
            confirm: '',
            type:''
        };

        vm.activate = activate;

        ////////////////
        activate();

        function signupClick() {
            var competitionName = "";
            if($location.host()=="ebay.startupcup.com"){
              competitionName = "eBay StartUp Cup Challenge";
            }
            else if($location.host()=="orange.startupcup.com"){
              competitionName = "Orange StartUp Cup Challenge";
            }
            else {
              competitionName = "StartUp Cup Challenge";
            }

            vm.registrationFailed = false;
            vm.registering = true;
                var credentials = {
                  email: vm.user.email,
                  firstname:vm.user.firstname,
                  lastname:vm.user.lastname,
                  password:vm.user.password,
                  userType:"entrepreneur",
                  location:$location.host(),
                  competitionName:competitionName,
                  competitionId:vm.user.competitionId
                };
                AuthService.register(credentials, function(result){
                    vm.registering = false
                    if(result==200){
                      vm.registrationFailed = false;
                      $mdToast.show(
                          $mdToast.simple()
                          .content($filter('triTranslate')('Please check your email to confirm your account'))
                          .position('top right')
                          .action($filter('triTranslate')('Login'))
                          .highlightAction(true)
                          .hideDelay(0)
                      ).then(function() {
                          $state.go('authentication.login');
                      });
                    }
                    else if (result == 422){

                      vm.registrationFailed = true;
                      vm.errorMessage = "Error Occurred! Email Already Registered";
                    }
                    else if (result == 500){
                      vm.registrationFailed = true;
                      vm.errorMessage = "Error Occurred! Connectivity Problem";
                    }

                });



      }

        function activate(){
            vm.resource = AdminUIService.query({},"competitions");
            vm.resource.then(function(res){
              console.log(res);
              vm.competitions = res.data.data;
            },
            function(err){

            }
          )
            $scope.$watch('vm.user.email', function () {

                vm.registrationFailed = false;


            });
        }

    }

})();
