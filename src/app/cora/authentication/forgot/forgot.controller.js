(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .controller('ForgotController', ForgotController);

    /* @ngInject */
    function ForgotController($scope, $state, $mdToast, $filter, $http, triSettings, AuthService, SiteInfoService) {

        var vm = this;
        vm.siteURL = SiteInfoService.getSiteInfo().siteURL;
        vm.competitionName = SiteInfoService.getSiteInfo().competitionName;
        vm.triSettings = triSettings;
        vm.user = {
            email: ''
        };
        vm.resetClick = resetClick;

        ////////////////

        function resetClick() {
          var credentials = {
            email:vm.user.email,
            userType:"entrepreneur"
          }
          AuthService.forgot(credentials, function(result, user){
            if(result == 200){
              $mdToast.show(
                  $mdToast.simple()
                  .content($filter('triTranslate')('Your password reset link has been email to you.'))
                  .position('bottom right')
                  .action($filter('triTranslate')('Login'))
                  .highlightAction(true)
                  .hideDelay(0)
              ).then(function() {
                  $state.go('authentication.login');
              });
            }
            else {
              vm.resetError = true;
              $state.go('authentication.forgot');
            }


          });
        }
    }
})();
