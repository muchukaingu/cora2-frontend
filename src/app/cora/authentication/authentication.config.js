(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .config(moduleConfig);

    /* @ngInject */
    function moduleConfig($stateProvider, triMenuProvider) {

        $stateProvider
        .state('authentication', {
            abstract: true,
            views: {
                'root': {
                    templateUrl: 'app/cora/authentication/layouts/authentication.tmpl.html'
                }
            }
        })
        .state('authentication.login', {
            url: '/login',
            templateUrl: 'app/cora/authentication/login/login.tmpl.html',
            controller: 'LoginController',
            controllerAs: 'vm'
        })
        .state('authentication.signup', {
            url: '/signup',
            templateUrl: 'app/cora/authentication/signup/entrepreneur-signup.tmpl.html',
            controller: 'SignupController',
            controllerAs: 'vm'
        })
        .state('authentication.lock', {
            url: '/lock',
            templateUrl: 'app/cora/authentication/lock/lock.tmpl.html',
            controller: 'LockController',
            controllerAs: 'vm'
        })
        .state('authentication.forgot', {
            url: '/forgot',
            templateUrl: 'app/cora/authentication/forgot/forgot.tmpl.html',
            controller: 'ForgotController',
            controllerAs: 'vm'
        })
        .state('authentication.reset', {
            url: '/reset',
            templateUrl: 'app/cora/authentication/reset/reset.tmpl.html',
            controller: 'ResetController',
            controllerAs: 'vm'
        })
        .state('triangular.profile', {
            url: '/profile',
            templateUrl: 'app/cora/authentication/profile/profile.tmpl.html',
            controller: 'ProfileController',
            controllerAs: 'vm',
            data: {
                layout: {
                    sideMenuSize: 'off'
                }
            }
        });


    }
})();
