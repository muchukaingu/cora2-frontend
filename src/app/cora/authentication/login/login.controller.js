(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .controller('LoginController', LoginController);

    /* @ngInject */
    function LoginController($state, triSettings, AuthService, $scope, $location, SiteInfoService) {

        var vm = this;
        vm.siteURL = SiteInfoService.getSiteInfo().siteURL;
        vm.competitionName = SiteInfoService.getSiteInfo().competitionName;
        vm.emailVerified = $location.search().verified;
        vm.loginClick = loginClick;
        vm.logOut = logOut;
        vm.activate = activate;
        vm.userTypes = [
          {label:"Entrepreneur",value:"entrepreneur"},
          {label:"Judge",value:"judge"},
          {label:"Mentor",value:"mentor"},
          {label:"Manager",value:"manager"},
          {label:"Administrator",value:"administrator"}
        ];

        vm.triSettings = triSettings;
        // create blank user variable for login form
        vm.user = {
            email: '',
            password: ''
        };

        activate();

        ////////////////

        function loginClick() {
            //$state.go('triangular.dashboard');
            var credentials = {
              email: vm.user.email,
              password:vm.user.password,
              userType:vm.user.userType
            }
            // if(AuthService.logIn(credentials)){
            //   $state.go('triangular.admin-crops');
            // }
            AuthService.logIn(credentials, function(result, user){
              console.info("login result", result);
              if(result==true){
                if(user.type == "admin"){
                  $state.go('triangular.admin-countries');
                }

                switch(user.type){
                  case "admin":
                    $state.go('triangular.admin-countries');
                    break;
                  case "manager":
                    $state.go('triangular.competition-competitionstages');
                    break;
                  case "entrepreneur":
                    $state.go('triangular.profile');
                    break;
                }

              }
              else {
                console.log(result);
                vm.loginFailed = true;
                if(result.code == "LOGIN_FAILED_EMAIL_NOT_VERIFIED"){
                  vm.errorMessage = "Login failed. Verify your email.";
                }
                else if(result.code==500){
                  vm.errorMessage = "Connectivity error occurred. Please contact contact@griffinworx.org";
                }
                else {
                  vm.errorMessage = "Login failed. Please try again.";
                }
                console.info("Login Error", result)


              }
            });


        }

        function logOut(){
          AuthService.logOut({},function(result){
              $state.go('authentication.login');
          });
        }

        function activate(){
          $scope.$watch('vm.user.email', function () {

              vm.loginFailed = false;


          });
          $scope.$watch('vm.user.password', function () {

              vm.loginFailed = false;


          });
          $scope.$watch('vm.user.userType', function () {

              vm.loginFailed = false;


          });
        }

    }
})();
