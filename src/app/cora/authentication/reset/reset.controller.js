(function() {
    'use strict';

    angular
        .module('app.cora.authentication')
        .controller('ResetController', ResetController);

    /* @ngInject */
    function ResetController($scope, $state, $mdToast, $filter, $http, triSettings, $location, AuthService, SiteInfoService) {
        var vm = this;
        vm.resetFailed = false;
        vm.errorMessage = "Error Occurred! Please request another reset."
        vm.triSettings = triSettings;
        vm.siteURL = SiteInfoService.getSiteInfo().siteURL;
        vm.competitionName = SiteInfoService.getSiteInfo().competitionName;
        vm.user = {
            email: ''
        };
        vm.resetClick = resetClick;


        ////////////////

        function resetClick() {
          var auth = $location.search();
          console.log(auth);
          var credentials = {
            password:vm.user.password,
            confirmation:vm.user.confirmation,
            auth_token: $location.search().access_token
          }


          // if(AuthService.logIn(credentials)){
          //   $state.go('triangular.admin-crops');
          // }
          AuthService.reset(credentials, function(result, user){
            if(result == 200){
              $mdToast.show(
                  $mdToast.simple()
                  .content($filter('triTranslate')('Your password has successfully been reset.'))
                  .position('bottom right')
                  .action($filter('triTranslate')('Login'))
                  .highlightAction(true)
                  .hideDelay(0)
              ).then(function() {
                  $state.go('authentication.login');
              });

            }
            else {
              vm.resetError = true;
              //$state.go('authentication.forgot');
            }


          });


        }
    }
})();
