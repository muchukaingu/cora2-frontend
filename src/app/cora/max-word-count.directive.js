(function() {
    'use strict';

    /**
     * @ngdoc directive
     * @name app.cora.directive:dynamicModel
     *
     * @description
     * This directive creates a dynamic ngModel
     */

    angular
        .module('app.cora')
        .directive('maxWordCount', maxWordCount);

    /* @ngInject */
    function maxWordCount($compile, $parse){
        return {
           require: 'ngModel',
           link: function(scope, elm, attrs, ctrl) {

               //For DOM -> model validation
               var maxLimit = attrs.maxWordCount;
               ctrl.$parsers.unshift(function(value){

                   var valid = true;

                   if(value){
                       valid = value.split(' ').length <= maxLimit;
                   }

                   ctrl.$setValidity('maxWordCount', valid);
                   return valid ? value : undefined;
               });

               //For model -> DOM validation
               ctrl.$formatters.unshift(function(value) {

                   var valid = true;

                   if(value){
                       valid = value.split(' ').length <= maxLimit;
                   }

                   ctrl.$setValidity('maxWordCount',valid);
                   return value;
               });

           }
        };
  }

})();
