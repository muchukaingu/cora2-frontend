(function () {
  'use strict';
  angular
      .module('app.cora.admin-ui')
      .controller('ReferenceSelectorController', ReferenceSelectorController);

  function ReferenceSelectorController ($timeout, $q, ReferenceService, AdminUIService, $scope) {
    var vm = this;
    vm.readonly = false;
    vm.selectedItem = null;
    vm.searchText = null;
    vm.querySearch = querySearch;
    vm.objects = [];
    vm.object_refs = [];
    vm.selectedObjects = [];
    vm.transformChip = transformChip;
    vm.loadReferences = loadReferences;
    vm.activate = activate;



     var pendingSearch, cancelSearch = angular.noop;
     var lastSearch;


     vm.asyncContacts = [];
     vm.filterSelected = true;

     vm.querySearch = querySearch;
     vm.delayedQuerySearch = delayedQuerySearch;

    vm.activate();


    /**
     * @ngdoc method
     * @name activate
     * @methodOf app.cora.admin-ui.controller:ReferenceSelectorController
     * @description
     * This is the initialisation method for the controller and it executes all functions that are needed when bootstrapping the view
     *
     *
     *

    */

    function activate() {
        //vm.loadReferences();
        console.log(vm.refs);

    }


    /**
     * Async search for contacts
     * Also debounce the queries; since the md-contact-chips does not support this
     */
    function delayedQuerySearch(query,type) {
      console.log(type + "xxxxxxx");
      if ( !pendingSearch || !debounceSearch() )  {
          cancelSearch();

          return pendingSearch = $q(function(resolve, reject) {
            // Simulate async search... (after debouncing)
            cancelSearch = reject;




            type = type.substring(0, type.length-1);
            vm.resource = AdminUIService.query({},type);
            vm.resource.then(
              function(res){

                vm.objects = res.data.data;

                vm.objects = vm.objects.map(function (object) {
                  object._lowername = object.name.toLowerCase();
                  return object;
                });
                console.log(vm.objects);

                resolve( vm.querySearch(query) );




          });
        });

        return pendingSearch;
      }
    }

    function refreshDebounce() {
      lastSearch = 0;
      pendingSearch = null;
      cancelSearch = angular.noop;
    }

    /**
     * Debounce if querying faster than 300ms
     */
    function debounceSearch() {
      var now = new Date().getMilliseconds();
      lastSearch = lastSearch || now;

      return ((now - lastSearch) < 300);
    }



    /**
     * Return the proper object when the append is called.
     */
    function transformChip(chip) {
      // If it is an object, it's already a known chip

      if (angular.isObject(chip)) {
        console.log(chip);

        return chip;


      }

      // Otherwise, create a new one
      return { name: chip, type: 'new' }
    }

    /**
     * Search for crops.
     */
    function querySearch (query) {

      var results = query ? vm.objects.filter(createFilterFor(query)) : [];
      console.log(results);
      return results;

    }

    /**
     * Create filter function for a query string
     */
    function createFilterFor(query) {
      var lowercaseQuery = angular.lowercase(query);

      return function filterFn(object) {

        return (object._lowername.indexOf(lowercaseQuery) === 0)
      };

    }

    function loadReferences() {
      vm.resource = ReferenceService.getReferences();
      vm.resource.then(
        function(references){
          vm.provinces = references["provinces"];
          vm.districts = references["districts"];
          vm.towns = references["towns"];
          vm.wards = references["wards"];
          vm.markets = references["markets"];
          vm.packagings = references["packagings"];
          vm.crops = references["crops"];
        },
        function(error){

        }
      )
    }
  }
})();
