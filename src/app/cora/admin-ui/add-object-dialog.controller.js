(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name app.cora.admin-ui.controller:AddObjectDialogController
     *
     * @description
     * This is a controller for the dialog for adding Objects
     */

    angular
        .module('app.cora.admin-ui')
        .controller('AddObjectDialogController', AddObjectDialogController);

    /* @ngInject */
    function AddObjectDialogController($q, $state, $mdDialog, $scope, data, ReferenceService, AdminUIService, AlertService) {
        var vm = this;
        vm.editMode = false;
        vm.references = []; //Used for looking up the names of other types of objects like provinces
        vm.childSets = []; //Used for displaying childsets to user
        vm.childSetHeadings = []; //Used for displaying childsets to user
        vm.inputs = [];
        vm.selects = [];
        vm.chips = [];
        vm.dateControls = [];
        vm.predefinedSelects = [];
        vm.embeddedArrayFiles = [];
        vm.fields = [];
        vm.arrayFields = [];
        vm.model = data.model;
        vm.selected = data.selected;
        vm.mode = data.mode;
        vm.object = {}; //View's scope object
        vm.object_refs = [];


        //Controller's API
        vm.activate = activate;
        vm.cancel = cancel;
        vm.createDetailFields = createDetailFields;
        vm.createFields = createFields;
        vm.getReferences = getReferences;
        vm.getPlural = getPlural;
        vm.generateChildrenSets = generateChildrenSets;
        vm.hide = hide;
        vm.setReference = setReference;
        vm.initAddOrEditMode = initAddOrEditMode;



        vm.activate();

        /////////////////////////

        /**
         * @ngdoc method
         * @name createFields
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the UI controls for the dialog in add or edit mode
         *
         *
         *

        */

        function createFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model.properties, function(config, column){


                  //Debug
                  if(column == "type"){
                    console.log(config);
                  }
                  if(config.hasOwnProperty("editor_index")){


                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";
                      var objectType = config.data_source.substring(4, config.data_source.length-2);

                      switch(objectType) {
                          case "province":
                              refs = vm.provinces;
                              break;
                          case "district":
                              refs = vm.districts;
                              break;
                          case "town":
                              refs = vm.towns;
                              break;
                          case "ward":
                              refs = vm.wards;
                              break;
                          case "market":
                              refs = vm.markets;
                              break;
                          case "packagings":
                              refs = vm.packagings;
                              break;
                          case "crop":
                              refs = vm.crops;
                              break;
                          case "advertiser":
                              refs = vm.advertisers;
                              console.log(refs);
                              break;

                          default:
                              console.log("error");
                      }




                      vm.selects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});

                      //If we are in edit mode set the value of the selected reference, so that the name is visible to the user
                      if(vm.selected && vm.selected.length == 1){
                          angular.forEach(vm.selects, function(select){
                            vm.object.ref=vm.object[select.name]; //temporal fix. It will be a problem when there are multiple reference types. Find a fix to allow dynamic ng-model
                          })
                      }


                    }
                    else if (config.hasOwnProperty("choices")){
                      var refs = config.choices;
                      console.log(config.choices);
                      vm.predefinedSelects.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});

                    }
                    else if (config.type == "date") {
                      if(vm.mode=="edit"){
                        console.log(vm.object[column]);
                        vm.object[column] = new Date(vm.object[column]);
                        console.log(vm.object[column]);
                      }
                      vm.dateControls.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }
                    else {
                      vm.inputs.push({name: column, editor_index:config.editor_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }

                  }
                });

                angular.forEach(vm.model.relations, function(config, column){
                 if (config.type == "belongsTo" || config.type == "hasOne" ){
                    var refs = [];
                    var pluralModel = getPlural(config.model);
                    vm.resource = AdminUIService.query({},pluralModel);
                    vm.resource.then(
                      function(res){
                        refs = res.data.data;
                        vm.selects.push({name: column+"Id", refs:refs, label:column});
                      },
                      function(err){

                      }
                    )

                  }

                });


                resolve (vm);
                console.log(vm.inputs);
            });


        }


        /**
         * @ngdoc method
         * @name createDetailFields
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method traverses the model information received from the API and generates the information to display on the dialog in view mode
         *
         *
         *

        */

        function createDetailFields() {

          return $q(function(resolve, reject) {

                angular.forEach(vm.model.properties, function(config, column){
                  // console.log(config);
                  // console.log(column);
                  if(config.hasOwnProperty("detail_index")){

                    if(config.type == "LLForeignKey"){
                      var refs = [];
                      var ref_model = "";
                      var objectType = config.data_source.substring(4, config.data_source.length-2);

                      switch(objectType) {
                          case "province":
                              refs = vm.provinces;
                              break;
                          case "district":
                              refs = vm.districts;
                              break;
                          case "town":
                              refs = vm.towns;
                              break;
                          case "ward":
                              refs = vm.wards;
                              break;
                          case "market":
                              refs = vm.markets;
                              break;
                          case "packaging":
                              refs = vm.packagings;
                              break;
                          case "crop":
                              refs = vm.crops;
                              break;
                          default:
                              console.log("error");
                      }
                      vm.fields.push({type: config.type, name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length, refs: refs});
                    }
                    else {
                      vm.fields.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                    }

                  }
                  else if (config.type=="array"){
                    vm.embeddedArrayFiles.push({type: config.type,name: column, detail_index:config.detail_index, label: config.label, required: config.required?"true":"false", maxlength:config.max_length});
                  }
                });

                angular.forEach(vm.model.relations, function(config, column){

                  if (config.type == "belongsTo" || config.type == "hasOne" ){
                    vm.fields.push({type: config.type, name: column, detail_index:99, label: column});
                  }
                  else if (config.type == "hasMany"){
                    vm.arrayFields.push({type: config.type, name: column, detail_index:99, label: column});
                  }


                });

                resolve (vm);
            });

        }


        /**
         * @ngdoc method
         * @name setReference
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method sets the reference on the model because ng-model does not provide the ability to be dynamically updated
         *
         *
         *

        */

        function setReference (select){
          vm.object[select.name] = vm.object.ref;
          console.log(select);

        }


        /**
         * @ngdoc method
         * @name generateChildrenSets
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method goes through the set fields and matches them with the references arrays to display user friendly lists to the user
         *
         *
         *

        */

        function generateChildrenSets (){
          for(var field in vm.model.relations){

              console.log(field);
              vm.childSetHeadings.push(field);
              var childrenArray = vm[index];
              var children = []; // storing this object's actual children
              angular.forEach(childrenArray, function(child){
                if(child[vm.model.objectType.toLowerCase()+"_ref"]==vm.object.id){
                  children.push(child);


                }
              });

              vm.childSets.push(children);
              vm.object[field] = children;



          }
          console.log(vm.childSets);

        }


        /**
         * @ngdoc method
         * @name activate
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This is the initialisation method for the controller and it executes all functions that are needed when bootstrapping the view
         *
         *
         *

        */

        function activate() {
            vm.initAddOrEditMode();
            vm.createDetailFields();
            vm.getReferences();
            vm.controlsResource = vm.createFields();
            vm.controlsResource.then(
              function(){
                console.log("showing modal");
              },
              function(){}
            );








        }

        /**
         * @ngdoc method
         * @name getReferences
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method initialises the references for the view from other entities e.g. provinces, districts etc
         *
         *
         *

        */

        function getReferences() {




        }

        /**
         * @ngdoc method
         * @name initAddOrEditMode
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method sets the mode to add or edit when the view initialises. It also sets the scope object
         *
         *
         *

        */

        function initAddOrEditMode(){

          //If an item has been selected, then we are in edit mode and we must bind the fields of the view the selected object
          console.log(vm.mode);
          if(vm.mode == "edit"){
            vm.object = vm.selected[0];

          }
          else {
            vm.object = {};
            vm.selected = [];
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name toggleMode
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method switches the views mode between add and edit mode
         *
         *
         *

        */

        function toggleMode(){
          if(vm.editMode){
            vm.editMode = false;
          }
          else {
            vm.editMode = true;
          }
        }

        /**
         * @ngdoc method
         * @name hide
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and returns the scope object to the calling controller
         *
         *
         *

        */

        function hide() {


          angular.forEach(vm.object, function(value, key){
            console.log(key);

          });




          $mdDialog.hide(vm.object);
        }

        /**
         * @ngdoc method
         * @name cancel
         * @methodOf app.cora.admin-ui.controller:AddObjectDialogController
         * @description
         * This method dismisses the modal and does not return the scope object to the calling controller
         *
         *
         *

        */

        function cancel() {
            $mdDialog.cancel();
        }



        function getPlural(model){
          if (model.slice(-1)=="y"){
            return model = model.substring(0, model.length-1)+"ies".toLowerCase();

          }
          else {

            return model = model.toLowerCase() + "s";


          }
        }
    }
})();
