(function() {
    'use strict';

    angular
        .module('app.cora.admin-ui')
        .service('AdminUIService', AdminUIService);

    /* @ngInject */
    function AdminUIService($q, $http, SessionService, coraSettings, $resource, $cookies, APIService, $state){

        //set specific endpoint for this entity
        var endPoint = "";
        var currentCompetition = "";
        var parent = "";
        var user = SessionService.getUser();
        if(!user == null && user.type !== "admin"){
          angular.forEach(user.centre.competitions, function(competition){
            if (competition.active){
              currentCompetition = competition.id

            }
          });
        }


        function getParent(state){
          var current = state.current.name;
          return current.substring(current.indexOf(".")+1, current.indexOf("-"));
        }



        function getEndPoint(type, action){
          var objectType = "";
          if (type=="state"){

            var state = $state;
            var current = state.current.name;
            parent = getParent(state);



            if(parent == "competition" && action == "query"){
              objectType = current.substring(current.indexOf("-")+1, current.length)+"?filter[where][competitionId]="+currentCompetition;
            }
            else{
              objectType = current.substring(current.indexOf("-")+1, current.length);
            }


          }
          else {
            objectType = type;
          }




          /*if (objectType.slice(-1)=="y" && !parent=="competition"){
            objectType = objectType.substring(0, objectType.length-1)+"ies".toLowerCase();
            console.log(objectType);
          }
          else {
            if (!parent=="competition"){
              objectType = objectType.toLowerCase() + "s";
            }

          }*/

          return objectType;

        }






        this.query = function(object, type){
          parent = getParent($state);
          console.log(parent);
          if (parent == "competition" || parent == "users" || "triangular."){
            return APIService.query(object, getEndPoint(type, "query"));
          }
          return APIService.queryAll(object, getEndPoint(type, "query"));

        }

        this.queryAll = function(object, type){
          return APIService.queryAll(object, getEndPoint(type, "query"));
        }

        this.save = function(object, type){
          return APIService.save(object, getEndPoint(type, "save"));
        }

        this.update = function(object, type){
          return APIService.update(object, getEndPoint(type, "update"));
        }

        this.delete = function(object, type){
          return APIService.delete(object, getEndPoint(type, "delete"));
        }

  }

})();
