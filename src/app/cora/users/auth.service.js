(function() {
    'use strict';

    angular
        .module('app.cora.users')
        .service('AuthService', AuthService);

    /* @ngInject */
    function AuthService($q, $http, SessionService, coraSettings, RoleStore, $cookies, APIService, $stateParams, triMenu){
        //get base URL from settings
        var baseURL = coraSettings.API;
        var currentUser = SessionService.getUser();
        //set specific endpoint for this entity
        var endPoint = "";



        function getEndPoint(type, action){
            switch (action) {
              case "login":
                return type+"s/login?include=user";
                break;
              case "logout":
                type = currentUser.type;
                return type+"s/logout";
                break;
              case "reset":
                return "reset-password?access_token="+type;
                break;
              case "forgot":
                return type+"s/reset";
                break;
              default:
                return type+"s";
                break;

            }
            return type+"s/login";

        }






        this.query = function(object, type){
          return APIService.query(object, getEndPoint(type));
        }

        this.save = function(object, type){
          return APIService.save(object, getEndPoint(type));
        }

        this.update = function(object, type){
          return APIService.update(object, getEndPoint(type));
        }

        this.delete = function(object, type){
          return APIService.delete(object, getEndPoint(type));
        }

        /**
        * Check whether the user is logged in
        * @returns boolean
        */
        this.isLoggedIn = function isLoggedIn(){
          return SessionService.getUser() !== null;
        };

        /**
        * Log in
        *
        * @param credentials
        * @returns {*|Promise}
        */
        this.logIn = function(credentials, callback){
          var resource = APIService.save(credentials, getEndPoint(credentials.userType, "login"));
          resource.then(function(response){
              var headers = response.headers;
              var status = response.status;
              var user = response.data.data.user;
              var accessToken = response.data.data.id;


              console.log(status);
              if(status!==200){
                console.log($cookies.csrftoken);

              }
              else {
                var user = user;
                setupMenus(user);
                user.displayName = user.firstname + " " + user.lastname;
                //get the default/ current competition using 'active' flag
                if(user.gender=="male"){
                  user.avatar = 'assets/images/avatars/avatar-5.png';
                }
                else if (user.gender=="female"){
                  user.avatar = 'assets/images/avatars/avatar-6.png';
                }
                else {
                  user.avatar = 'assets/images/avatars/avatar-9.jpg';
                }

                SessionService.setUser(user);
                SessionService.setAccessToken(accessToken);

                console.log(SessionService.getAccessToken());
                currentUser = user;
                //$stateParams.competitionid =
                callback(true, user);

              }


            },
            function(error){
              callback(error.data==null?error.data = {code:500}:error.data.error, {});
            });
        };

        /**
        * Log out
        *
        * @returns {*|Promise}
        */
        this.logOut = function(credentials, callback){
          var resource = APIService.save(credentials, getEndPoint(credentials.userType, "logout"));
          resource.then(function(response){
              SessionService.destroy();
              triMenu.removeAllMenu();
              callback(true);

            },
            function(error){
              SessionService.destroy();
              triMenu.removeAllMenu();
              callback(true);
            }
          );

        };

        /**
        * Register
        *
        * @returns {*|Promise}
        */
        this.register = function(credentials, callback){
          var resource = APIService.save(credentials, getEndPoint(credentials.userType, "register"));
          resource.then(function(response){
              callback(200);

            },
            function(error){
              console.log(error)
              callback(error.status);
            }
          );

        };

        /**
        * Reset
        *
        * @returns {*|Promise}
        */
        this.reset = function(credentials, callback){
          var resource = APIService.save(credentials, getEndPoint(credentials.auth_token, "reset"));
          resource.then(function(response){
              callback(200);

            },
            function(error){
              console.log(error)
              callback(error.status);
            }
          );

        };


        /**
        * Reset
        *
        * @returns {*|Promise}
        */
        this.forgot = function(credentials, callback){
          var resource = APIService.save(credentials, getEndPoint(credentials.userType, "forgot"));
          resource.then(function(response){
              callback(200);

            },
            function(error){
              console.log(error)
              callback(error.status);
            }
          );

        };


        this.hasPermission = function(permission) {
            var deferred = $q.defer();
            var hasPermission = false;

            // check if user has permission via its roles
            angular.forEach(currentUser.roles, function(role) {
                // check role exists
                if(RoleStore.hasRoleDefinition(role)) {
                    // get the role
                    var roles = RoleStore.getStore();

                    if(angular.isDefined(roles[role])) {
                        // check if the permission we are validating is in this role's permissions
                        if(-1 !== roles[role].validationFunction.indexOf(permission)) {
                            hasPermission = true;
                        }
                    }
                }
            });

            // if we have permission resolve otherwise reject the promise
            if(hasPermission) {
                deferred.resolve();
            }
            else {
                deferred.reject();
            }

            // return promise
            return deferred.promise;
        }

        this.getUsers = function() {
            return $http.get('app/permission/data/users.json');
        }

        function setupMenus(user){
          //Setup menus for different user RoleStore

          if (user && user && user.type == "entrepreneur"){
            /*triMenu.addMenu(
              {
                name: 'My Menu',
                icon: 'zmdi zmdi-folder',
                type: 'dropdown',
                permission: 'viewAdminUI',
                priority: 1.1,
                children: [
                  {
                    name: 'Submissions',
                    state: 'triangular.entrepreneur-submissions',
                    type: 'link'
                  },
                  {
                    name: 'Coaching Sessions',
                    state: 'triangular.entrepreneur-coachingsessions',
                    type: 'link'
                  },
                  {
                    name: 'Pitching Sessions',
                    state: 'triangular.entrepreneur-pitchingsessions',
                    type: 'link'
                  }


                ]
            }
          )*/
          }
          else if (user && user.type == "manager"){
            triMenu.addMenu(
              {
                name: 'Competition Management',
                icon: 'zmdi zmdi-folder',
                type: 'dropdown',
                permission: 'viewAdminUI',
                priority: 1.1,
                children: [
                  {
                    name: 'Stages',
                    state: 'triangular.competition-competitionstages',
                    type: 'link'
                  },
                  {
                    name: 'Entrepreneurs',
                    state: 'triangular.competition-entrepreneurs',
                    type: 'link'
                  },
                  {
                    name: 'Judges',
                    state: 'triangular.competition-judges',
                    type: 'link'
                  },
                  {
                    name: 'Mentors',
                    state: 'triangular.competition-mentors',
                    type: 'link'
                  },
                  {
                    name: 'Coaching Sessions',
                    state: 'triangular.competition-coachingsessions',
                    type: 'link'
                  },
                  {
                    name: 'Pitching Sessions',
                    state: 'triangular.competition-pitchingsessions',
                    type: 'link'
                  },
                  {
                    name: 'Forms',
                    state: 'triangular.competition-submissionforms',
                    type: 'link'
                  }


                ]
            });

          }
          else if (user && user.type == "admin"){
            triMenu.addMenu(
              {
                name: 'Admin',
                icon: 'zmdi zmdi-folder',
                type: 'dropdown',
                permission: 'viewAdminUI',
                priority: 1.1,
                children: [
                  {
                    name: 'Countries',
                    state: 'triangular.admin-countries',
                    type: 'link'
                  },
                  {
                    name: 'Centres',
                    state: 'triangular.admin-centres',
                    type: 'link'
                  },
                  {
                    name: 'Competitions',
                    state: 'triangular.admin-competitions',
                    type: 'link'
                  },
                  {
                    name: 'Stages',
                    state: 'triangular.admin-stages',
                    type: 'link'
                  },
                  {
                    name: 'Submissions',
                    state: 'triangular.admin-submissions',
                    type: 'link'
                  }

                ]
            });


            triMenu.addMenu(
              {
                name: 'Users',
                icon: 'zmdi zmdi-face',
                type: 'dropdown',
                permission: 'viewAdminUI',
                priority: 1.1,
                children: [
                  {
                    name: 'Administrators',
                    state: 'triangular.users-administrators',
                    type: 'link'
                  },
                  {
                    name: 'Managers',
                    state: 'triangular.users-managers',
                    type: 'link'
                  },
                  {
                    name: 'Entrepreneurs',
                    state: 'triangular.users-entrepreneurs',
                    type: 'link'
                  },
                  {
                    name: 'Judges',
                    state: 'triangular.users-judges',
                    type: 'link'
                  },
                  {
                    name: 'Mentors',
                    state: 'triangular.users-mentors',
                    type: 'link'
                  }


                ]
            });
          }
        }

  }

})();
