(function() {
    'use strict';

    angular
        .module('app.cora')
        .run(coraRun);

    /* @ngInject */
    function coraRun($rootScope, $cookies, $state, PermissionStore, RoleStore, UserService, AuthService, SessionService, $stateParams, triMenu) {
        // normally this would be done at the login page but to show quick
        // demo we grab username from cookie and login the user


        //SessionService.setAccessToken("L7YtPxw6gWtc0Tu96OXbJY3Ja4ROJew4DBu281MMQbDfciax4KzMztkRnT7YkxLT");
        /*if(angular.isDefined(cookieUser)) {
            UserService.login(cookieUser);
        }
        */
        var user = SessionService.getUser();
        console.info("Current User", user);
        if(!user==null && user.hasOwnProperty("centre") && user.centre.hasOwnProperty("competitions")){
          angular.forEach(user.centre.competitions, function(competition){
            if (competition.active){
              $stateParams.competitionid = competition.id
              console.info("stateParams",$stateParams);
            }
          });
        }

        //Setup menus for different user RoleStore

        if (user && user && user.type == "entrepreneur"){
          /*triMenu.addMenu(
            {
              name: 'My Menu',
              icon: 'zmdi zmdi-folder',
              type: 'dropdown',
              permission: 'viewAdminUI',
              priority: 1.1,
              children: [
                {
                  name: 'Submissions',
                  state: 'triangular.entrepreneur-submissions',
                  type: 'link'
                },
                {
                  name: 'Coaching Sessions',
                  state: 'triangular.entrepreneur-coachingsessions',
                  type: 'link'
                },
                {
                  name: 'Pitching Sessions',
                  state: 'triangular.entrepreneur-pitchingsessions',
                  type: 'link'
                }


              ]
          }
        )*/
        }
        else if (user && user.type == "manager"){
          triMenu.addMenu(
            {
              name: 'Competition Management',
              icon: 'zmdi zmdi-folder',
              type: 'dropdown',
              permission: 'viewAdminUI',
              priority: 1.1,
              children: [
                {
                  name: 'Stages',
                  state: 'triangular.competition-competitionstages',
                  type: 'link'
                },
                {
                  name: 'Entrepreneurs',
                  state: 'triangular.competition-entrepreneurs',
                  type: 'link'
                },
                {
                  name: 'Judges',
                  state: 'triangular.competition-judges',
                  type: 'link'
                },
                {
                  name: 'Mentors',
                  state: 'triangular.competition-mentors',
                  type: 'link'
                },
                {
                  name: 'Coaching Sessions',
                  state: 'triangular.competition-coachingsessions',
                  type: 'link'
                },
                {
                  name: 'Pitching Sessions',
                  state: 'triangular.competition-pitchingsessions',
                  type: 'link'
                },
                {
                  name: 'Forms',
                  state: 'triangular.competition-submissionforms',
                  type: 'link'
                }


              ]
          });

        }
        else if (user && user.type == "admin"){
          triMenu.addMenu(
            {
              name: 'Admin',
              icon: 'zmdi zmdi-folder',
              type: 'dropdown',
              permission: 'viewAdminUI',
              priority: 1.1,
              children: [
                {
                  name: 'Countries',
                  state: 'triangular.admin-countries',
                  type: 'link'
                },
                {
                  name: 'Centres',
                  state: 'triangular.admin-centres',
                  type: 'link'
                },
                {
                  name: 'Competitions',
                  state: 'triangular.admin-competitions',
                  type: 'link'
                },
                {
                  name: 'Stages',
                  state: 'triangular.admin-stages',
                  type: 'link'
                },
                {
                  name: 'Submissions',
                  state: 'triangular.admin-submissions',
                  type: 'link'
                }

              ]
          });


          triMenu.addMenu(
            {
              name: 'Users',
              icon: 'zmdi zmdi-face',
              type: 'dropdown',
              permission: 'viewAdminUI',
              priority: 1.1,
              children: [
                {
                  name: 'Administrators',
                  state: 'triangular.users-administrators',
                  type: 'link'
                },
                {
                  name: 'Managers',
                  state: 'triangular.users-managers',
                  type: 'link'
                },
                {
                  name: 'Entrepreneurs',
                  state: 'triangular.users-entrepreneurs',
                  type: 'link'
                },
                {
                  name: 'Judges',
                  state: 'triangular.users-judges',
                  type: 'link'
                },
                {
                  name: 'Mentors',
                  state: 'triangular.users-mentors',
                  type: 'link'
                }


              ]
          });
        }






    }
})();
