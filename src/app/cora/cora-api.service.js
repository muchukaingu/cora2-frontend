(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name app.cora.service:APIService
     *
     * @description
     * This service provides high-level abstraction for all API calls.
     */

    angular
        .module('app.cora')
        .service('APIService', APIService);

    /* @ngInject */
    function APIService($q, $http, SessionService, coraSettings, $resource, $cookies){
        //get base URL from settings
        var baseURL = coraSettings.API;
        var authtoken = ""
      

        /**
  			 * @ngdoc method
  			 * @name query
  			 * @methodOf app.cora.service:APIService
  			 * @description
  			 * This method queries the API using the GET method
  			 *
  			 * @param {Object} object specific object to retrieve. Leave blank to return all objects
         *
         * @returns {$promise} $http promise

  			*/

        this.query = function(object, endpoint) {
            authtoken = SessionService.getAccessToken();
            return $http({
                url: baseURL + endpoint,
                method: 'GET',
                params: object,
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    //"X-CSRFToken" : csrftoken
                    "Authorization": authtoken
                }
            });
        };

        /**
  			 * @ngdoc method
  			 * @name queryAll
  			 * @methodOf app.cora.service:APIService
  			 * @description
  			 * This method queries the API using the GET method and embeds all relations
  			 *
  			 * @param {Object} object specific object to retrieve. Leave blank to return all objects
         *
         * @returns {$promise} $http promise

  			*/

        this.queryAll = function(object, endpoint) {

            return $http({
                url: baseURL + endpoint+"/alldata",
                method: 'GET',
                params: object,
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    //"X-CSRFToken" : csrftoken
                    "Authorization": authtoken
                }
            });
        };



        /**
         * @ngdoc method
         * @name save
         * @methodOf app.cora.service:APIService
         * @description
         * This method is for creating instances of entities via the API
         *
         * @param {Object} object object to create
         *
         * @returns {$promise} $http promise

        */

        this.save = function(object, endpoint) {
            return $http({
                url: baseURL + endpoint,
                method: 'POST',
                data: object,
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    //"X-CSRFToken" : csrftoken
                    "Authorization": authtoken

                }
            });
        };

        /**
         * @ngdoc method
         * @name update
         * @methodOf app.cora.service:APIService
         * @description
         * This method is for updating instances of entities via the API
         *
         * @param {Object} object object to update
         *
         * @returns {$promise} $http promise

        */

        this.update = function(object, endpoint) {
            return $http({
                url: baseURL + endpoint,
                method: 'PUT',
                data: object,
                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    "Authorization": authtoken
                }
            });
        };


        /**
         * @ngdoc method
         * @name delete
         * @methodOf app.cora.service:APIService
         * @description
         * This method is for deleting instances of entities via the API
         *
         * @param {Object} object object to delete
         *
         * @returns {$promise} $http promise

        */

        this.delete = function(object, endpoint) {
            return $http({
                url: baseURL + endpoint + "/" + object.id,
                method: 'DELETE',

                headers: {
                    "Content-Type": "application/json;charset=utf-8",
                    "Authorization": authtoken
                }
            });
        };

  }

})();
