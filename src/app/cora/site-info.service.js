(function() {
    'use strict';

    /**
     * @ngdoc service
     * @name app.cora.service:SiteInfoService
     *
     * @description
     * This service provides high-level abstraction for all API calls.
     */

    angular
        .module('app.cora')
        .service('SiteInfoService', SiteInfoService);

    /* @ngInject */
    function SiteInfoService($location){



        /**
  			 * @ngdoc method
  			 * @name getSiteInfo
  			 * @methodOf app.cora.service:SiteInfoService
  			 * @description
  			 * This method gets and returns site info from the $location service
  			 *
  			 * @param {Object} object specific object to retrieve. Leave blank to return all objects
         *
         * @returns {$promise} $http promise

  			*/

        this.getSiteInfo = function() {



            var siteURL = $location.host();
            var competitionName = "";
            if($location.host()=="ebay.startupcup.com"){
              competitionName = "eBay StartUp Cup Challenge";
            }
            else if($location.host()=="orange.startupcup.com"){
              competitionName = "Orange StartUp Cup Challenge";
            }
            else {
              competitionName = "StartUp Cup Challenge";
            }

            return  {
              siteURL:siteURL,
              competitionName:competitionName

            };


        };

  }

})();
