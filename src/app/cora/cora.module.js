(function() {
    'use strict';

    angular
        .module('app.cora', [
            'app.cora.admin-ui',    
            'app.cora.authentication',
            'app.cora.users'

        ]);
})();
