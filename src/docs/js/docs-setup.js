NG_DOCS={
  "sections": {
    "Documentation": "Web App Documentation"
  },
  "pages": [
    {
      "section": "Documentation",
      "id": "AdminController",
      "shortName": "AdminController",
      "type": "function",
      "moduleName": "triAngular",
      "shortDescription": "Handles the admin view",
      "keywords": "admin admincontroller documentation function handles triangular view"
    },
    {
      "section": "Documentation",
      "id": "AdminController",
      "shortName": "AdminController",
      "type": "function",
      "moduleName": "triAngular",
      "shortDescription": "Handles the admin view",
      "keywords": "admin admincontroller documentation function handles triangular view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:AddMarketDialogController",
      "shortName": "AddMarketDialogController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Add Markets Dialog",
      "keywords": "add addmarketdialogcontroller admin-ui api app controller dialog distr documentation getdistricts gettowns cora markets method queries towns"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:CropsController",
      "shortName": "CropsController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Crops View",
      "keywords": "action activate addcrop admin-ui api app binds boardcasts broadcasts controller crops cropscontroller data default deletecrop documentation editcrop event filters getcrops initial initialises cora method queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:DistrictsController",
      "shortName": "DistrictsController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Districts View",
      "keywords": "action activate adddistrict admin-ui api app binds boardcasts broadcasts controller data default deletedistrict districts districtscontroller documentation editdistrict event filters getdistricts initial initialises cora method queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:FarmersController",
      "shortName": "FarmersController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Farmers View",
      "keywords": "action activate addfarmer admin-ui api app binds boardcasts broadcasts controller data default deletefarmer documentation editfarmer event farmers farmerscontroller filters getfarmers initial initialises cora method queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:MarketsController",
      "shortName": "MarketsController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Markets View",
      "keywords": "action activate addmarket admin-ui api app binds boardcasts broadcasts controller data default deletemarket documentation editmarket event filters getmarkets initial initialises cora markets marketscontroller method queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:PackagingController",
      "shortName": "PackagingController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Packaging View",
      "keywords": "action activate addpackaging admin-ui api app binds boardcasts broadcasts controller data default deletepackaging documentation editpackaging event filters getpackaging initial initialises cora method packaging packagingcontroller queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:ProvincesController",
      "shortName": "ProvincesController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Provinces View",
      "keywords": "action activate addprovince admin-ui api app binds boardcasts broadcasts controller data default deleteprovince documentation editprovince event filters getprovinces initial initialises cora method provinces provincescontroller queries querying removefilter removes returns setting view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:TownsController",
      "shortName": "TownsController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Towns View",
      "keywords": "action activate addtown admin-ui api app binds boardcasts broadcasts controller data default deletetown documentation edittown event filters gettowns initial initialises cora method queries querying removefilter removes returns setting towns townscontroller view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.admin-ui.controller:WardsController",
      "shortName": "WardsController",
      "type": "controller",
      "moduleName": "app.cora.admin-ui",
      "shortDescription": "This is a controller for the Wards View",
      "keywords": "action activate addward admin-ui api app binds boardcasts broadcasts controller data default deleteward documentation editward event filters getwards initial initialises cora method queries querying removefilter removes returns setting view wards wardscontroller"
    },
    {
      "section": "Documentation",
      "id": "app.cora.service:AlertService",
      "shortName": "AlertService",
      "type": "service",
      "moduleName": "app.cora",
      "shortDescription": "This service provides high-level abstraction for all API calls.",
      "keywords": "abstraction alertservice api app bottom calling calls creates display documentation high-level cora mdtoast message method object parent provided service showalert toast"
    },
    {
      "section": "Documentation",
      "id": "app.cora.service:APIService",
      "shortName": "APIService",
      "type": "service",
      "moduleName": "app.cora",
      "shortDescription": "This service provides high-level abstraction for all API calls.",
      "keywords": "$http abstraction api apiservice app blank calls create creating delete deleting documentation entities high-level instances leave cora method object objects promise queries query retrieve return save service specific update updating"
    },
    {
      "section": "Documentation",
      "id": "app.cora.service:ChipService",
      "shortName": "ChipService",
      "type": "service",
      "moduleName": "app.cora",
      "shortDescription": "This service provides high-level abstraction for all API calls creating chips",
      "keywords": "abstraction api app calls chips chipservice creating documentation high-level cora service"
    },
    {
      "section": "Documentation",
      "id": "app.cora.users.controller:AdministratorsController",
      "shortName": "AdministratorsController",
      "type": "controller",
      "moduleName": "app.cora.users",
      "shortDescription": "This is a controller for the Administrators View",
      "keywords": "action activate addadministrator administrators administratorscontroller api app binds boardcasts broadcasts controller data default deleteadministrator documentation editadministrator event filters getadministrators initial initialises cora method queries querying removefilter removes returns setting users view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.users.controller:AgentsController",
      "shortName": "AgentsController",
      "type": "controller",
      "moduleName": "app.cora.users",
      "shortDescription": "This is a controller for the Agents View",
      "keywords": "action activate addagent agents agentscontroller api app binds boardcasts broadcasts controller data default deleteagent documentation editagent event filters getagents initial initialises cora method queries querying removefilter removes returns setting users view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.users.controller:AggregatorsController",
      "shortName": "AggregatorsController",
      "type": "controller",
      "moduleName": "app.cora.users",
      "shortDescription": "This is a controller for the Aggregators View",
      "keywords": "action activate addaggregator aggregators aggregatorscontroller api app binds boardcasts broadcasts controller data default deleteaggregator documentation editaggregator event filters getaggregators initial initialises cora method queries querying removefilter removes returns setting users view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.users.controller:Analytics_UsersController",
      "shortName": "Analytics_UsersController",
      "type": "controller",
      "moduleName": "app.cora.users",
      "shortDescription": "This is a controller for the Analytics_Users View",
      "keywords": "action activate addanalytics_user analytics_users analytics_userscontroller api app binds boardcasts broadcasts controller data default deleteanalytics_user documentation editanalytics_user event filters getanalytics_users initial initialises cora method queries querying removefilter removes returns setting users view"
    },
    {
      "section": "Documentation",
      "id": "app.cora.users.controller:FarmersController",
      "shortName": "FarmersController",
      "type": "controller",
      "moduleName": "app.cora.users",
      "shortDescription": "This is a controller for the Farmers View",
      "keywords": "action activate addfarmer api app binds boardcasts broadcasts controller data default deletefarmer documentation editfarmer event farmers farmerscontroller filters getfarmers initial initialises cora method queries querying removefilter removes returns setting users view"
    }
  ],
  "apis": {
    "Documentation": true
  },
  "__file": "_FAKE_DEST_/js/docs-setup.js",
  "__options": {
    "startPage": "/api",
    "scripts": [
      "js/angular.min.js",
      "js/angular-animate.min.js",
      "js/marked.js"
    ],
    "styles": [],
    "title": "API Documentation",
    "html5Mode": true,
    "editExample": true,
    "navTemplate": false,
    "navContent": "",
    "navTemplateData": {},
    "loadDefaults": {
      "angular": true,
      "angularAnimate": true,
      "marked": true
    }
  },
  "html5Mode": true,
  "editExample": true,
  "startPage": "/api",
  "scripts": [
    "js/angular.min.js",
    "js/angular-animate.min.js",
    "js/marked.js"
  ]
};